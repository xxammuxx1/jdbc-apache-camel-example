package com.mycompany.camelsampleproject;

import org.apache.camel.CamelContext;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.impl.DefaultCamelContext;
import org.apache.camel.support.SimpleRegistry;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.log4j.BasicConfigurator;

// TODO 1 - configure CData JDBC Driver dependency in pom.xml

/**
 *
 * @author cdata
 */
public class App {
    public static void main(String[] args) throws Exception{
        
        BasicConfigurator.configure();
        
        BasicDataSource basic = new BasicDataSource();
        // TODO 2.1 - configure driver class name
        basic.setDriverClassName("com.databricks.client.jdbc.Driver");
        // TODO 2.2 - configure connect URL
        basic.setUrl("jdbc:databricks://<Server Hostname>:443;HttpPath=<Http Path>;Property1=Value1;Property2=Value2;");
                
        SimpleRegistry reg = new SimpleRegistry();
        reg.bind("myDataSource", basic);
        
        CamelContext context = new DefaultCamelContext(reg);

        context.addRoutes(new RouteBuilder() {
            @Override
            public void configure() {
                from("timer://foo?repeatCount=1")
                    // TODO 3.3 - configure SQL Query for route body
                    .setBody(constant("SELECT * FROM TABLE"))
                    .to("jdbc:myDataSource")
                    .marshal().json(true)
                    // TODO 3.3 - configure output file path and name
                    .to("file:C:\\Users\\USER\\Documents?fileName=TABLE.json");
            }
        });
        
        context.start();
        Thread.sleep(10000);
        context.stop();
    }
}
